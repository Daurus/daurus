# Daurus

Репозиторий для обучения

- https://www.docker.com/products/docker-desktop/ - Docker
- https://github.com/rnwood/smtp4dev/wiki/Installation - SMTP сервер для докера

Перед работой в VScode, установить плагины:
- Code Runner
- Python
- autopep8
- Pylance
- Bluloco Dark Theme
- Material Icon Theme

Горячие клавиши VScode

Ctrl + D                            # Массовое выделение строк
Ctrl + Shit + K                     # Удалить строку(и)
Shift + Alt + (Вниз/Вверх)          # Копировать строку(и)
Alt + (Вниз/Вверх)                  # Перемещать строку(и) в коде
