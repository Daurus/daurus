# Lesson211 Модуль DateTime

from datetime import date
from datetime import time
from datetime import datetime


print(type(date))

my_date = date(2024, 4, 15)

print(my_date)

print(my_date.day)
print(my_date.year)
print(my_date.month)

print(my_date.isocalendar())


my_time = time(18, 10, 45)

print(my_time)

print(my_time.hour)
print(my_time.microsecond)
print(my_time.minute)
print(my_time.second)


my_datetime = datetime(2222, 12, 10, 18, 10, 45)
print(my_datetime)
print(my_datetime.isoformat())
print(my_datetime.now().microsecond)
