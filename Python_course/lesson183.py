# Lesson183 Практика декораторов


def decorator_function(orginal_fn):
    def wrapper_function(*args, **kwargs):
        # some actions before execution of the original_fn
        print("Executed before function")
        result = orginal_fn(*args, **kwargs)
        # some actions after execution of the original_fn
        print("Executed after function")

        return result

    return wrapper_function


@decorator_function
def my_fuction(a, b):
    print("This is my function!")
    return (a, b)


result = my_fuction("arbuz", 50)
print(result)
