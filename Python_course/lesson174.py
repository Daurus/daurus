# Lesson174 Задача на экземпляры класса

# Создать классы Image
# У каждого экземпляра Image должно быть 3 атрибута -resolution, -title, -extension Тип - строка.
# Должен быть метод resize, менять разрешение изображения вы должны просто менять значение атрибута resolution
# Создать несколько экземпляров класса image и вызвать метод resize

class House():
    """Описание дома"""

    def __init__(self, street, number):
        """свойства дома"""
        self.street = street
        self.number = number
        self.age = 0

    def build(self):
        """строит дом"""
        print("Дома на улице " + self.street +
              " под номером " + str(self.number) + " построен.")

    def age_of_house(self, year):
        """возраст дома"""
        self.age += year


# House1 = House("Московская", 20)
# House2 = House("Московская", 21)

# print(House1.street)
# print(House1.number)

# House1.build()

# print(House1.age)

# House1.age_of_house(5)
# print(House1.age)

class ProspectHouse(House):
    """дома на проспекте"""

    def __init__(self, prospect, number):
        super().__init__(self, number)
        self.prospect = prospect


PrHouse = ProspectHouse("Ленина", 5)
print(PrHouse.prospect)
