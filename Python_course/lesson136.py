# Lesson135 Задача - Решение

# 1. Создать функцию Image_info с одним параметром dict
# функция ожидает словарь, в котором должно быть 2 ключа image_id, image_title
# функция должна возвращать строк такого вида 'image 'my cat' has id 4136"
# если хотя бы одиного из ключей в словаре нет, функция должна генерировать ошибку TypeError
# Вызовие функцию и корректно обработайте ошибку в случае возникновения


def image_info(img):
    if ('image_id' not in img) or ('image_title' not in img):
        raise TypeError("Keys image_id and image_title must be present")
    return f"Image {img['image_title']} has id {img['image_id']}"


print(image_info({'image_title': 'My cat', 'image_id': 123}))

try:
    print(image_info({'image_id': 123}))

except TypeError as e:
    print(e)
