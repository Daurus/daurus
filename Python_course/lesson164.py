# Lesson164 Пример с сокращенным for in

# Пример 1

my_set = {1, 10, 15}

new_set = {val * val for val in my_set}

# new_set = set()

# for val in my_set:
#     new_set.add(val*val)

print(new_set)

print(my_set)


my_scores = {
    'a': 10,
    'b': 7,
    'c': 14
}

# Пример 2 Обычный for in
# scores = {}

# for key, value in my_scores.items():
#     scores[key] = value * 10

# Пример 3 Сокращенный for in

scores = {k: v * 10 for k, v in my_scores.items()}

print(scores)

print(my_scores)
