# Lesson147 Тернарный оператор

# Переменная = Выражение_1 if Условие else Выражение_2

my_number = 21.5

print('is int') if type(my_number) is int else print("is not int")

if type(my_number) is int:
    print("is int")
else:
    print("is not int")

# Проверка отправки
# send_img(img) if img.get['is_processed'] else process_and_send_img(img)

product_qty = 10
print("in stock" if product_qty > 0 else "out of stock")

temp = +20
weather = "hot" if temp > 19 else "cold"
print(weather)
