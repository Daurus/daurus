# Lesson209 Работа с csv файлами

import csv

with open('test.csv', 'w') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(['user_id', 'user_name', 'comments_qty'])
    writer.writerow([5235, 'Rustam', 1352])
    writer.writerow([999, 'Alex', 777])
    writer.writerow([165, 'Andrew', 73])
