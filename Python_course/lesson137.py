# Lesson137 Оператор распаковки

# Извлечение значений, и присвоение переменным
# list, tuple - упорядоченная последовательность элементов

my_fruits = ['apple', 'banana', 'lime']

my_apple, my_banana, my_lime = my_fruits

print(my_fruits)

print(my_apple)

# my_apple = my_fruits[0]
# my_banana = my_fruits[1]
# my_lime = my_fruits[2]

# * при расапковке
my_fruits2 = ['apple', 'banana', 'lime']

my_apple, *remaining_fruits = my_fruits2

print(my_apple)
print(remaining_fruits)
