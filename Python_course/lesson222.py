# Lesson222 Задача - проверка пароля

import re

# Создать функцию для проверки пароля (чек пасс)
# Пароль минимум 8 символов
# буквы в нижнем/верхнем регистре, цифры, спецсимволы
# Ввести пароль в терминале и проверить его(создать несколько паттернов, условие И)

# importing re library


def main():
    passwd = input("Введите пароль: ")
    reg = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{6,20}$"

    # compiling regex
    pat = re.compile(reg)

    # searching regex
    mat = re.search(pat, passwd)

    # validating conditions
    if mat:
        print("Password is valid.")
    else:
        print("Password invalid !!")


main()
