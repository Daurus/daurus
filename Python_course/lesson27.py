# Lesson27 - Объявление и присвоение переменных

my_number = 10
print(my_number)

my_boolean = True
print(my_boolean)

my_str = 'str'
print(my_str)

my_array = [1, 2, 3]
print(my_array)

my_tuple = {1, 2, 3}
print(my_tuple)
