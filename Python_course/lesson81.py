# Lesson81 Изменение объектов в PYTHON

# Перменные могут ссылаться на 1 объект в памяти

my_number = 10
print(id(my_number))

other_number = 10
print(id(other_number))

print(id(10))

# Изменение объекта = Создание новго объекта
first_num = 10
second_num = first_num
print(id(first_num))
print(id(second_num))

second_num += 5
print(first_num)
print(second_num)

print(id(first_num))
print(id(second_num))
