# Lesson 59 - Практика словарей

my_disk = {}

# print(id(my_disk))

# print(type(my_disk))

my_disk['brand'] = 'Samsung'

my_disk['price'] = 80

# print(my_disk)

# print(id(my_disk))

print(my_disk.items())  # Кортежи

print(my_disk.keys())

print(my_disk.popitem())

print(my_disk.get('type'))

print(my_disk.get('type',))

print(my_disk.get('type'))
