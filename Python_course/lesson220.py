# Lesson220 Сохранение паттерна в отдельном объекте

import re

my_string = "My name is Rustam. Rustam is instructor"

# res = re.search('^M.*name', my_string)

# res = re.search('R....m\.$', my_string)

my_pattern = re.compile(r'R....m')

print(my_pattern)

print(my_pattern.findall(my_string))
