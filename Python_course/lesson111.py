# Lesson111 Унарные и бинарные операторы

# Унарные
# - my_number

# + my_number # Переменная содержит число (позитивное). Конвертация лог. значения в целое число

# not is_activated # Лог. оператор отрицания

my_num = 10

print(+my_num)

my_bool = True
print(+my_bool)

my_bool = False
print(+my_bool)

print(not my_num)

# Бинарные операторы
a = 5
a + b
a += 5
a == b
a and b

#Инфиксная запись
a = True 
a + break
a += 5
a or break
a > b 