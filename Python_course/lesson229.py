# Lesson229 Запись данных в таблицу SQLITE

import sqlite3

DB_NAME = "sqlite_db.db"

courses = [
    (351, "JavaScript course", 415, 100),
    (614, "C++ course", 161, 10),
    (721, "Java full course", 100, 35)

]

with sqlite3.connect(DB_NAME) as sqlite_conn:
    sql_request = "INSERT INTO courses VALUES(?, ?, ?, ?)"
    # sqlite_conn.execute(sql_request, (251, "Python couese", 100, 30))
    for course in courses:
        sqlite_conn.execute(sql_request, course)
    sqlite_conn.commit()
