# Lesson127 Лямбда функции

# def mult (a, b):
#     return a * b

def mult(a, b): return a * b


print(mult(2, 5))


def greeting(greet):
    return lambda name: f"{greet}, {name}!"


morning_greeting = greeting('Good Morning')

print(morning_greeting('Rustam'))

evening_greeting = greeting("Good Evening")

print(evening_greeting('Rustam'))
