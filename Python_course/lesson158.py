# Lesson158 задача с фильтром

print(isinstance(True, bool))
print(isinstance(True, int))
print(isinstance(True, object))
print(int.__subclasses__())


def filter_list(list_to_filter, value_type):
    def check_element_type(elem):
        return isinstance(elem, value_type)
    return filter(check_element_type, list_to_filter)


res = list(filter_list([1, 10, 'abc', True, 5.5], int))
print(res)


def filter_list(list_to_filter, value_type):
    def check_element_type(elem):
        return isinstance(elem, value_type)
    return filter(check_element_type, list_to_filter)


res = list(filter_list([1, 10, 'abc', True, 5.5], int))
print(res)
