# Lesson163 Сокращенный цикл for in

# Выражение for Элемент in Последовательность if Условие

all_nums = [-3, 1, 0, 10, -20, 5]

# positive_nums = []

# Вариант 4 Сокращенный for in
positive_nums = [num for num in all_nums if num > 0]

# Вариант 3 Формирование нового списка с фильтацией
# for num in all_nums:
#     if num > 0:
#         positive_nums.append(num)

# Вариант 2 В 1 строку
# absolute_nums = [abs(num) for num in all_nums]

# Вариант 1
# absolute_nums = []

# for num in all_nums:
#     absolute_nums.append(abs(num))

# print(absolute_nums)

print(positive_nums)

print(all_nums)
