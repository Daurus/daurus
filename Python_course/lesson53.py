# Lesson53 Словари

# Dict порядок не играет роли

my_motrobike = {
    'brand': 'Ducati',
    'price': 25000,
    'engine_vol': 1.2
}

other_motrobike = {
    'engine_vol': 1.2,
    'price': 25000,
    'brand': 'Ducati'
}

print(my_motrobike)
print(other_motrobike)
print(my_motrobike == other_motrobike)
