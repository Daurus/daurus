# Lesson135 Задача

# 1. Создать функцию Image_info с одним параметром dict
# функция ожидает словарь, в котором должно быть 2 ключа image_id, image_title
# функция должна возвращать строк такого вида 'image 'my cat' has id 4136"
# если хотя бы одиного из ключей в словаре нет, функция должна генерировать ошибку TypeError
# Вызовие функцию и корректно обработайте ошибку в случае возникновения


# def image_info(dict = {a, b}):
#     {'image_id': a,
#      'image_title': b
#      }
#     return "Image", image_id, "My cat has id", image_title


# print(image_info(4136, "barsik"))
