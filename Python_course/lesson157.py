# Lesson157 Задачи решение

# Задача1
# Создать функцию dict_to_list, которая будет конвертировать словарь в список кортежей
# Функция должна принимать словарь,а возвращать список кортежей, в каждом кортеже должны быть пары (key, value) из словаря
# Если значение ключа - целое число, то его нужно умножить на 2, перед добавлением в кортеж

# Задача2
# Создать функцию filter_list, которая будет фильтровать список
# У функции должно быть 2 параметра - список и тип значения
# Функция должна вернуть новый список, в котором останутся только значения того типа, который был передан в вызове функции вторым аргументом
# Функцию можно будет вызвать например так:
# filter_list([35, true, 'abc', 10], int) и получить [35, 10]

def dict_to_list(dict_to_convet):
    list_for_convertion = []
    for k, v in dict_to_convet.items():
        if type(v) == int:
            v *= 2
        list_for_convertion.append((k, v))
    return list_for_convertion


print(dict_to_list({'a': 5, 'b': [], 'c': 100}))

# Задача 2


def filter_list(list_to_filter, value_type):
    filtered_list = []
    for element in list_to_filter:
        # if type(element) == value_type:
        if isinstance(element, value_type):
            filtered_list.append(element)
    return filtered_list


print(isinstance(True, bool))
print(isinstance(True, int))
print(isinstance(True, object))

print(filter_list([35, True, 'abc', 10], int))
print(filter_list([35, True, 'abc', 10], str))
print(filter_list([35, True, 'abc', 10], bool))
