# Lesson83 Как избежать изменения копий

from copy import deepcopy

info = {
    'name': 'Rustam',
    'is_instructor': True,
    'reviews': []
}

info_copy = info.copy()

info_copy['reviews_qty'] = 5

info_copy['reviews'].append('Great course!')

info_deepcopy = deepcopy(info)

info_deepcopy['reviews'].append('Great course!1111')

print(info_copy)

print(info)

print(info_deepcopy)
