# Lesson146 Решение задачи автором

# 1 Создать функцию route_info которой будет передаваться словарь
# 2 Если в слове есть ключ Distance и его значение целое число, верните строку "Distance to your destination is <distance"
# Иначе если в словаре есть ключи speed и time верните строку "Distance to your destination is <speed*time>"
# Иначе верните строку "No distance info is available"
# Вызвать функцию несколько раз с разными аргументами

def route_info(route):
    if ('distance' in route) and (type(route['distance']) == int):
        return f"Distance to your destination is {route['distance']}"

    if ('speed' in route) and ('time' in route):
        return f"Distance to your destination is {route['speed'] * route['time']}"

    return "No distance info is available"


print(route_info({'speed': 20, 'time': 3}))
# Distance to your destination is 60

print(route_info({'distance': 15}))
# Distance to your destination is 15

print(route_info({'my_speed': 305}))
# No distance info is available
