# Lesson145 Задача условные инструкции

# 1 Создать функцию route_info которой будет передаваться словарь
# 2 Если в слове есть ключ Distance и его значение целое число, верните строку "Distance to your destination is <distance"
# Иначе если в словаре есть ключи speed и time верните строку "Distance to your destination is <speed*time>"
# Иначе верните строку "No distance info is available"
# Вызвать функцию несколько раз с разными аргументами

dict = {
    "distance": 3.0,
    "time": 5,
    "speed": 5
}


def route_info(distance, time, speed):
    if ('distance' in dict) and (type(distance) is int):
        return f"Distance to your destination is {distance}"
    elif ('speed' in dict) and ('time' in dict):
        return f"Distance to your destination is {speed * time}"
    else:
        return "No distance info is availible"


print(route_info(**dict))
