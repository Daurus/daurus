# Lesson173 Магический метод класса init - практика


class Comment:
    def __init__(self, text):
        self.text = text
        self.votes_qty = 0

    def upvote(self, qty):
        self.votes_qty += qty

    def reset_votes_qty(self):
        self.votes_qty = 0


first_comment = Comment("First comment")

# print(first_comment)
# print(type(first_comment))
# print(first_comment.__dict__)
# print(first_comment.votes_qty)
# print(first_comment.text)
# print(dir(first_comment))

first_comment.upvote(5)
print(first_comment.votes_qty)

first_comment.upvote(10)
print(first_comment.votes_qty)

first_comment.upvote = 10

second_comment = Comment("Second comment")
second_comment.upvote(2)
print(second_comment.votes_qty)

print(first_comment.__dict__)

first_comment.reset_votes_qty()
print(first_comment.votes_qty)
