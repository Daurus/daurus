# Lesson151 Циклы в Python

i = 10

print(i)
i *= 2

print(i)
i *= 2

print(i)
i *= 2

my_fruits = ['apple', 'banana', 'lime']

print(my_fruits[0])
print(my_fruits[1])
print(my_fruits[2])

# Циклы используются для перебора элементов последовательности (dict, list, tuple, set, range, str)

# Типы циклов: for ... in, while
