# Lesson172 Магический метод класса init

# Собственные атрибуты экземпляров определяются с помощью функции __init__

class Comment:
    def __init__(self, text):
        self.text = text
        self.votes_qty = 0

    def upvote(self):
        self.votes_qty += 1


first_comment = Comment("First comment")

print(first_comment.__dict__)
print(first_comment.votes_qty)
print(first_comment.text)
