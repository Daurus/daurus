# Lesson116 Ложные значения

# Значение, которе при приведении к логическому типу, дает false - является ложным

# int 0
# float 0.0
# comples 0j

# bool(value) -> False

print(bool(0))
print(bool(0.0))
print(bool(0j))

print(bool(None))

# Ложные значения
print(bool({}))
print(bool([]))
print(bool(()))
print(bool(set()))
print(bool(range(0)))
print(bool(''))
print(not not {})

# Правдивые значения
print(not not {'a': 10})
print(bool({'Rustam': 'Me'}))

# Конвертация в логическое значение не обязательно
my_list = []
print(len(my_list) >= 0)


if my_list:
    print("list has element")
