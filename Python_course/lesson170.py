# Lesson170 Классы и объекты


# На основании шаблонов создаются экземпляры
# Экземпляпы могут иметь собственный атрибуты
# Экземпляры наследуют атрибуты класса
# class ...

class Car:
    def move(self):
        print("Car is moving")

    def stop(self):
        print("Car stopped")


my_car = Car()

print(type(my_car))

print(isinstance(my_car, Car))

# Вызов метода экземпляра класса
my_car.move()

my_car.stop()
