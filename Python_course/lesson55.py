# Lesson55 Переменные словарей


my_motrobike = {
    'brand': 'Ducati',
    'price': 25000,
    'engine_vol': 1.2
}

key_name = 'brand'
my_motrobike[key_name] = 'BMW'
print(my_motrobike)


# Вложенный словарь
my_motrobike_2 = {
    'brand': 'Ducati',
    'engile_vol': 1.3,
    'price_info': {
        'price': 25000,
        'is_availible': True
    }
}
print(my_motrobike_2)


# Переменные для словарей
brand = 'Kawasaki'
bike_price = 8000
engine_volume = 2.0

my_motorbike_3 = {
    'brand': brand,
    'price': bike_price,
    'engine_vol': engine_volume,
}
print(my_motorbike_3)
