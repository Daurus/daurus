# Lesson92 Аргументы функции

# Ошибка отсутствия аргумента
def my_fn(a, b):
    a = a + 1
    c = a + b
    return c


# TypeError: my_fn() missing 1 required positional argument: 'b'
my_fn(10)
