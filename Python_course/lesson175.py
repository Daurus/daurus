# Lesson175 Задача решение

class Image:
    def __init__(self, resolution, extension, title):
        self.resolution = resolution
        self.extension = extension
        self.title = title

    def get_full_settings_picture(self):
        name = f"Имя картинки {self.title}, разрешение: {self.resolution}, а формат: {self.extension}"
        return name.title()

    def resize(self, new_resolution):
        """Устанавливает новое значение разрешения"""
        self.resolution = new_resolution


Image1 = Image("1920x1080", "JPG", "kartinka")

Image1.resize("1080x920")

print(Image1.get_full_settings_picture())
