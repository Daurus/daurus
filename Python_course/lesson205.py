# Lesson205 Задача на чтение файлов

from pathlib import Path
import os
import shutil

# Создать папку files
# Добавить 2 файла, first и second.txt в эту папку и записать в них по 2-3 строки текста
# Прочитать все строки первого файла
# Прочитайте строки второго файлла одна за другой
# Удалить оба файла
# Удалить папку files

basedir = os.path.abspath(os.getcwd())
print(basedir)

with open('Python_course/files/first.txt', 'w') as test_file1:
    test_file1.write("First string \n")
    test_file1.write("\n")
    test_file1.write("Second string \n")
    test_file1.write("Third string \n")

with open('Python_course/files/second.txt', 'w') as test_file2:
    test_file2.write("First string \n")
    test_file2.write("\n")
    test_file2.write("Second string \n")
    test_file2.write("Third string \n")


with open('Python_course/files/first.txt') as test_file1:
    print(test_file1.read())

with open('Python_course/files/second.txt') as test_file2:
    while True:
        # считываем строку
        line = test_file2.readline()
        # прерываем цикл, если строка пустая
        if not line:
            break
        # выводим строку
        print(line.strip())

# закрываем файл
test_file2.close
test_file1.close

shutil.rmtree('Python_course/files/')
