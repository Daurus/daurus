# Lesson182 Декораторы

# Декораторы - функция которая автоматически выполняет функции (доступны во внутр. и внешн. пакетах)

def decorator_function(orginal_fn):
    def wrapper_function():
        result = orginal_fn()

        return result

    return wrapper_function


@decorator_function
def my_fuction():
    print("This is my function!")


my_fuction()
