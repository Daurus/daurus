# Lesson65 Методы кортежей

# count(посчитать элементы) index(найти индекс элемента)

posts_ids = (151, 245, 752, 254)

print(posts_ids.count(245))
print(posts_ids.index(151))

# Конвертаця кортежа с список

posts_id = (151, 245)

posts_id_list = list(posts_id)
posts_id_list.append(351)

print(posts_id_list)

posts_id_list = tuple(posts_id_list)

print(posts_id_list)
