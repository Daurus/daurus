# Lesson152 Цикл for in

# for Элемент In Последовательность:

# Итерации по спискам
my_list = [True, 10, 'abc', {}]

for elem in my_list:
    print(elem)

# Итерации по кортежам
video_info = ('1920x1080', True, 27)

for elem1 in video_info:
    print(elem1)

# Итерации по словарям
my_object = {
    'x': 10,
    'y': True,
    'z': 'abc'
}

for key in my_object:
    print(key, my_object[key])
