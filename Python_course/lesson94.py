# Lesson94 Аргументы с ключевыми словами

def get_posts_info(name, posts_qty):
    info = f"{name} wrote {posts_qty} posts"
    return info


info = get_posts_info(name='Rustam', posts_qty=25)
print(info)
