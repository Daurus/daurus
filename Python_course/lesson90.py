# Lesson90 Задача функции

# Создать функцию merge_lists_to_dict
# у функции должно быть 2 параметра
# Функция должна объединять 2 списка используя встроенную функцию zip
# Конвертируйте объект zip в словарь и верните его из функции
# Вызовите функцию передав ей два списка в кач-ве аргументов

def merge_lists_to_dict(a, b):
    res_list_zip = zip(a, b)
    res_list = dict(res_list_zip)
    return res_list


first = ['keyword', True, 'lol']
second = ['password', 'lists', []]

merge_list = merge_lists_to_dict(first, second)

print(merge_list)
