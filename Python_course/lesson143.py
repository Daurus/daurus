# Lesson143 Инструкция if elif

# if Условие:
# Блок кода, выполняемый однократно, если условие1 правдиво
# elif Условие2:
# Блок кода, выполняемый, однократно, если условие1 ложно, а условие2 правдиво
# else:
# Блок кода, выполняемый, если условия ложны


my_number = 0

if my_number > 0:
    print(my_number, "is positive number")
elif my_number < 0:
    print(my_number, "is negative number")
else:
    print(my_number, "is zero")
