# Lesson93 Позиционные аргументы

def sum_nums(*args):
    print(args)
    print(type(args))
    print(args[0])
    return sum(args)


print(sum_nums(1, 55, 4))


def get_posts_info(name, posts_qty):
    info = f"{name} wrote {posts_qty} posts"
    return info


info = get_posts_info('Rustam', 27)
print(info)
