# Lesson139 Распаковка списка в позиционные аргументы

user_data = ['Bogdan', 23]


def user_info(name, comments_qty=0):
    if not comments_qty:
        return f"{name} has no comments"

    return f"{name} has {comments_qty} comments"


print(user_info(*user_data))
print(user_info(user_data[0], user_data[0]))
print(user_info(name=user_data[0], comments_qty=user_data[1]))

my_name, my_comments_qty = user_data

print(user_info(my_name, my_comments_qty))
