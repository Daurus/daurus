# Lesson80 Встроенная функция zip

fruits = ['apple', 'banana', 'lime', 'orange']

quantities = [100, 70, 50, 20]

fruit_qtys_zip = zip(fruits, quantities)

print(fruit_qtys_zip)

fruit_qtys_list = dict(fruit_qtys_zip)

print(fruit_qtys_list)
