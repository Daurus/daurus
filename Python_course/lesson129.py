# Lesson129 Обработка ошибок в Python


try:
    # Выполнение блока кода
    print(10 / 0.0)

except ZeroDivisionError:
    print("error - division by zero!")
    # Блок выполняется в случае возникновения ошибок в блоке try
    print('Continue...')
