# Lesson141 Инструкция if

# if Условие:
# Блок кода, выполняемый однократно, если условие правдиво


personal_info = {
    'age': 20,
}

if not personal_info.get('name'):
    print('Имя отсутствует')

print(not 0)

if 10 > 2:
    print(True)

num_one = 10
num_two = 5

if (num_one > 0 and
    num_two > 0 and
    isinstance(num_one, int) and
        isinstance(num_two, int)):
    print("Both number are ints and positive")
