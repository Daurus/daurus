# Lesson219 Модуль регулярных выражений RE

import re

my_string = "My name is Rustam."

# res = re.search('^M.*name', my_string)

res = re.search('R....m\.$', my_string)


print(res)

print(r'R....m\n.$')

print(res.span())
print(res.start())
print(res.end())
