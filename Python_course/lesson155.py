# Lesson155 Циклы for in для наборов

video_ids = {1435, 4317, 2761, 5721}

for id in video_ids:
    print(id)

# For in для строк
my_name = 'Rustam'

for chair in my_name:
    print(chair)

# For in для диапазонов (range)
for num in range(5):
    print(num)

# Начало, Конец, Шаг - Диапазон
for odd_num in range(3, 10, 2):
    print(odd_num)
