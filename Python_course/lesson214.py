# Lesson214 Модуль TIME


import time

start_time = time.time()

print(time.ctime(2345234523))

time.sleep(5)

end_time = time.time()

print(end_time - start_time)

start_time1 = time.time()

my_range = range(100000000)

print(my_range[1000])

end_time1 = time.time()

print("total duration of the operation: ", end_time1 - start_time1)
