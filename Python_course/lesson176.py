# Lesson176 Методы и экземпляры классов
class Comment:
    def __init__(self, text):
        self.text = text
        self.votes_qty = 0

    def upvote(self, qty):
        self.votes_qty += qty

    def reset_votes_qty(self):
        self.votes_qty = 0

# Привязанные методы (Bound methods)


first_comment = Comment("First comment")

print(first_comment.upvote)

print(Comment.upvote)

Comment.upvote()
