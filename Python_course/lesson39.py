# Lesson39 Числа с десятичной частью

average_price = 17.25

print(average_price.__pow__(3))

# Конвертация чисел
str_temperature = '14.5'
temperature = str(str_temperature)

print(temperature)
print(type(temperature))

# Округление чисел
average_sum = 20.22
print(round(average_sum))

rate = 0.51

print(round(rate))
print(type(round(rate)))
