# Lesson54 Словари 2

my_motrobike = {
    'brand': 'Ducati',
    'price': 25000,
    'engine_vol': 1.2
}

# Получение значение
print(my_motrobike['brand'])
print(my_motrobike['price'])


# Изменить значение
my_motrobike['price'] = 7000

print(my_motrobike['price'])

# Добавить значение
my_motrobike['is_new'] = True

print(my_motrobike)

# Удалить значение

del my_motrobike['engine_vol']

print(my_motrobike)
