# Lesson149 Задание тернарный оператор

my_img = ('1920', '1080')

print(f"{my_img[0]}x{my_img[1]}") if len(
    my_img) == 2 else print("Incorrect image formatting")

# Переписать с помощью if else
# Если длина строки больше 99 симв. пишем String is long, иначе String is short
