# Lesson43 Конвертация типов

# Явная конвертация типов
str()
int()
list()
set()
float()
tuple()


int_num = 5
float_num = 4.5

print(float_num + int_num)

bool_val = True
int_val = 7

print(int_val + bool_val)
