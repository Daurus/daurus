# Lesson82 Поведение изменяемых объектов Python

# Адреса объектов разные

my_list = [1, 2, 3]
print(id(my_list))

other_list = [1, 2, 3]
print(id(other_list))

print(id([1, 2, 3]))

# Изменения 1-го объекта
other_list.append(4)

print(my_list)
print(other_list)

# После копирования изменяемыех объектов
# Изменения отражаются на всех копиях
