# Lesson31 - Получение id объекта

# id(my_country)

my_number = 10
print(id(my_number))

other_number = my_number
print(id(my_number))

my_string = 'abc'
print(id(my_string))
