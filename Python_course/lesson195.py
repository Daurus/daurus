# Lesson195 Конвертация JSON в словарь

import json

json_str = '{"id":235, "brand": "Nike", "qty":84, "status": {"isforsale":true} }'

sneakers = json.loads(json_str)

print(sneakers['brand'])

print(type(sneakers))

print(sneakers['qty'])

print(sneakers['status']['isforsale'])

# Конвертация словаря в JSON
