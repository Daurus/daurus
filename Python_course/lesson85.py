# Lesson85 Функции

# Функция - блок кода, который можно выполнять многократно

# a = 5
# b = 3

# c = a + b
# print(c)

# a = 8
# b = 12

# c = a + b
# print(c)

# Реализция функцией


# def sum(a, b):
#     c = a + b
#     print(c)


# a = 5
# b = 3

# sum(a, b)

# a = 8
# b = 12

# sum(a, b)


def my_fn(a, b):
    a = a + 1
    c = a + b
    return c


res = my_fn(10, 3)
print(res)
