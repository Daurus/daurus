# Lesson154 Итерация по ключам метод Items

my_object = {
    'x': 10,
    'y': True
}

my_dict = {'id': 324, 'title': 'test'}

for item in my_object.items():
    key, value = item
    print(key, value)


for item in my_dict.items():
    key, value = item
    print(key, item)
