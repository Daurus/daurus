# Lesson28 - Статическая и динамическая типизация

# String a = 'abc'
# int b = 10
# b = 'xyz' # ERROR

def print_name(name):
    print(name)


print_name('Rustam')

print_name = 15

print_name('Rustam')
# Type Error

# 1. Всегда осмысленные названия переменных
# 2. Использовать существительные в переменных name, comments, new_photos
# 3. Названия методов и функций начинать с глагола, например get_data, create_request, merge_names
