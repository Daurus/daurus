# Lesson212 Практика Время


from datetime import datetime

my_datetime = datetime(2222, 12, 10, 18, 10, 45)

print(my_datetime.strftime('%d/%m/%Y'))
print(my_datetime.strftime('%d-%b-%Y %H%M%S'))

# Конвертация строки в время
date_str = '10/12/2022'

converted_date = datetime.strptime(date_str, '%d/%m/%Y')

print(converted_date)
