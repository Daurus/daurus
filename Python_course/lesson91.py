# Lesson91 Решение функции

# Создать функцию merge_lists_to_dict
# у функции должно быть 2 параметра
# Функция должна объединять 2 списка используя встроенную функцию zip
# Конвертируйте объект zip в словарь и верните его из функции
# Вызовите функцию передав ей два списка в кач-ве аргументов

def merge_lists_to_dict(list_one, list_two):
    zipped_seq = zip(list_one, list_two)
    return dict(zipped_seq)


res_one = merge_lists_to_dict(['a', 'b', 'c'], [10, True, []])

print(res_one)
