# Lesson40 Комплексные числа

complex_a = 10 + 7j
complex_b = 3 + 3j

sum = complex_a + complex_b
minus = complex_a - complex_b
multi = complex_a * complex_b

print(sum)
print(minus)
print(multi)

# (10 + 7j)(3 + 3j) = 30 + 30j + 21j - 21^2 = 9 + 51j
print(type(sum))
