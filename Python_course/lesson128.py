# Lesson128 практика Лямбда функции

def greeting(greet):
    def info(name):
        return f"{greet}, {name}!"
    return info


morning_greeting = greeting('Good Morning')

print(morning_greeting('Rustam'))

evening_greeting = greeting("Good Evening")

print(evening_greeting('Rustam'))
