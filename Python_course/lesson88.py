# Lesson88 Передача изменямых объектов

# При передачи изменямых объектов в функцию, функция может изменять внешние переменные

def increase_person_age(person):
    print(id(person))
    person['age'] += 1
    return person


person_one = {
    'name': 'Bob',
    'age': 21
}

print(id(person_one))

increase_person_age(person_one)
print(person_one['age'])
