# Lesson132 else в обработке ошибок


try:
    print('10' / 0)

except ZeroDivisionError as e:
    print(e)

except TypeError as e:
    print(e)

else:
    print('There was not error')

finally:
    print('Continue')
