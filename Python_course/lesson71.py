# Lesson71 Методы наборов

# add union remove differnce intersection discard clear copy update issubset pop issuperset

photo_sizes = {'1920x1080', '800x600'}

other_sizes = {'800x600', '1024x768'}

# Добавление набора
photo_sizes.add('1024x768')

print(photo_sizes)

# Объединение наборов
all_sizes = photo_sizes.union(other_sizes)

print(all_sizes)

# Пересечение наборов
common_s = photo_sizes.intersection(other_sizes)

print(common_s)

# Включение одного в другой набор
nums = {10, 5, 35}
other_nums = {20, 5, 12, 10, 35}

res = nums.issubset(other_nums)

print(res)

res_2 = other_nums.issuperset(nums)

print(res_2)
