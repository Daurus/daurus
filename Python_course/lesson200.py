# Lesson200 Методы класса Path

from pathlib import Path

file_path = Path('test.txt')

print([m for m in dir(file_path)
       if not m.startswith('_')])

print(Path.cwd())

print(Path('usr').joinpath('local').joinpath('bin'))

print(Path('User') / 'Vitya' / 'Kerambit')

# Пути в Windows

print(Path('C:/').joinpath('local').joinpath('bin'))

print(Path('C:/') / 'Vitya' / 'Kerambit')

print(Path('lesson195.py').exists())

print(Path('/Users/Daurus/OneDrive').exists())

print(Path('other.py').exists())

# Директория или файл

print(Path('lesson191.py').is_file())

print(Path('../python').is_file())

print(Path('../python').is_dir())

for f in Path('.').iterdir():
    print(f)
