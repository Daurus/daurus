# Lesson76 Практика диапазоны

my_range = range(5)

print(my_range)

print(type(my_range))

print(my_range[-1])

for n in my_range:
    print(n)

for n in range(12, 25, 5):
    print(n)

# список, кортеж, набор
print(list(range(12, 25, 5)))
print(tuple(range(12, 25, 5)))
print(set(range(12, 25, 5)))

# Словарь нельзя
# print(dict(range(12, 25, 5)))

my_range1 = range(10, 30, 3)
print(dir(my_range1))

print(my_range1.start)
print(my_range1.stop)
print(my_range1.step)

print(my_range1.index(13))
print(my_range1.index(19))

# нет элементы
# print(my_range1.index(20))

# Элемент есть в диапазон
print(my_range1.count(10))
