# Lesson210 Чтение csv

import csv

with open('test.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=';')
    writer.writerow(['user_id', 'user_name', 'comments_qty'])
    writer.writerow([5235, 'Rustam', 1352])
    writer.writerow([999, 'Alex', 777])
    writer.writerow([165, 'Andrew', 73])

with open('test.csv') as csv_file:
    reader = csv.reader(csv_file)
    csv_list = list(reader)
    print(csv_list)
    for line in reader:
        print(line)
    # for line in reader:
    #     print(line)
   # print(reader)
   # print(type(reader))
   # for line in reader:
   #     print(line)

print(reader.line_num)
