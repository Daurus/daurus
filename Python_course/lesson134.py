# Lesson134 Самостоятельное создание ошибок


def devide_nums(a, b):
    if b == 0:
        raise ValueError("second argument cant be 0")
    return a / b


print(devide_nums(1, 0))

try:
    devide_nums(10, 0)

except ValueError as e:
    print(e)

print('Continue')
