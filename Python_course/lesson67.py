# Lesson67 Наборы Set

# Набор - неупорядоченная последовательность элементов
# Набор содержит только уникальные элементы
# Наборы можно изменять
# В наборах сохраняют однотипные объекты

my_fruits = {'apple', 'banana', 'lime'}
posts_ids = {11, 22, 33, 33, 22}

print(my_fruits)

print(type(posts_ids))

print(len(my_fruits))

# 'set' object is not subscriptable
print(posts_ids[0])
