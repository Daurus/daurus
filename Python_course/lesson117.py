# Lesson117 Логические операторы

# not(Возвращает bool, унарный, префиксный), and, or (Возвращает значение 1-го из операндов)

# Для правдивых = False
# Для ложных = True
not 10
not 0  # True
not 'abc'
not ''  # True
not True
not None  # True
