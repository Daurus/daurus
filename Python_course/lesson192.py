# Lesson192 Встроенные модули

import math

print(math.pi)

print(math.pow(5, 7))

print(type(math))

print(dir(math))

# 200 модулей
# os - работа с файлами,  smtplib - отправка email, pprint - формат данных терминал
# time - действия со временем, zip - создавать, распаковывать архивы, calendar - даты
# sys- системные функции, csv - работа с csv, regext - регулярные выражения
# math  - математические операции, random - случайные числа, statistics - статистические операции

# help('modules')
# # help('calendar')
# help('zlib')
