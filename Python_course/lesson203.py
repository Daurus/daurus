# Lesson203 Практика файлы


with open('test.txt', 'a') as test_file:
    test_file.write("First string \n")
    test_file.write("\n")
    test_file.write("Second string \n")
    test_file.write("Third string \n")


with open('test.txt') as test_file:
    while True:
        line = test_file.readline()
        if not line:
            break
        print(line)

print(test_file.readline)
