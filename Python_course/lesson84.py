# Lesson84 Сохранение изменяемых объектов 2

from copy import deepcopy

info = {
    'name': 'Rustam',
    'is_instructor': True,
    'reviews': []
}


info_deepcopy = deepcopy(info)

info_deepcopy['reviews'].append('Great course!1111')
info_deepcopy['reviews'].append('Super')

info['new_key'] = 10

print(info)

print(info_deepcopy)
