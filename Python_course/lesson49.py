# Lesson49 Копирование списков

my_cars = ['BMW', 'Mercedes']

# Копирование slice
copied_cars = my_cars[:]

# Копирование copy(ПРЕДПОЧТИТЕЛЬНЫЙ)
copied_cars = my_cars.copy()

# Копирование list
copied_cars = list(my_cars)

copied_cars.append('Audi')

print(copied_cars)

print(my_cars)

print(id(my_cars)) == id(copied_cars)
