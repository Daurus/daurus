# Lesson196 Практика json

import json

json_str = '{"id":235, "brand": "Nike", "qty":84, "status": {"isforsale":true} }'

sneakers = json.loads(json_str)

json_from_dict = json.dumps(sneakers, indent=2)

print(json_from_dict)

print(type(json_from_dict))
