# Lesson56 Длина словаря

my_motrobike = {
    'brand': 'Ducati',
    'price': 25000,
    'engine_vol': 1.2
}

print(len(my_motrobike))

del (my_motrobike['price'])

print(len(my_motrobike))
