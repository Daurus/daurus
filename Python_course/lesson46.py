# Lesson46 Списки

my_fruits = ['apple', 'banana', 'lime']

my_int = ['151', '245', '762', '167']

my_list = [True, 'hi', 10]

empty_list = []
print(len(empty_list))

# Вывод значений списка
posts_ids = [151, 245, 762, 167]
print(posts_ids[0])
print(posts_ids[1])
print(posts_ids[-1])

# Изменение объекта
posts_ids[0] = 555
print(posts_ids)

# Удаление объекта
del posts_ids[-1]
print(posts_ids)

# Список словарей
users = [
    {
        'user_id': 134,
        'user_name': 'Max'
    },
    {
        'user_id': 831,
        'user_name': 'Bob'
    }
]

print(len(users))
print(users[1]['user_id'])

# Переменные в списках
my_fruit = 'apple'
other_fruit = 'banana'
new_fruit = 'tomato'\

all_fruits = [my_fruit, other_fruit, new_fruit]

print(all_fruits)

# Несуществующие элементы
post_ids(posts_ids[10])
# NameError: name 'post_ids' is not defined
