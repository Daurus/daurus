# Lesson121 Объединение словарей с помощью **

button_info = {
    'text': 'Buy',
    'width': 0

}

button_style = {
    'color': 'yellow',
    'width': 200,
    'height': 300
}

button = {
    **button_info,
    **button_style
}

# способ с |
button1 = button_info | button_style

print(button1)
print(button)
