# Lesson72 Практика методы наборов

my_set = {'abc', 'd', 'f', 'y'}
other_set = {'a', 'f', 'd'}

print(my_set.intersection(other_set))
print(other_set.intersection(my_set))

print(my_set.intersection('abcd'))

print(my_set.union(other_set))


print(other_set.issubset(my_set))

print(my_set.issuperset(other_set))

print(my_set.difference(other_set))

print(my_set.discard('d'))

print(my_set.remove('y'))

# Копирование
copied_set = my_set.copy()

my_set.add('t')

copied_set.add('l')

print(my_set)
print(copied_set)
