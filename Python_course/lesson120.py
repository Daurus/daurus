# Lesson120 Оператор распаковки словаря **

button = {
    'width': 200,
    'text': 'Buy'
}

red_button = {
    **button,
    'color': 'red'
}

green_button = {
    **button,
    'text': 'i"am green'
}

print(red_button)

print(button)

# Перезаписано значение
print(green_button)
