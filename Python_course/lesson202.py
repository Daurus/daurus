# Lesson202 Чтение файлов

with open('test.txt') as test_file:
    print(test_file.read())

with open('test.txt') as test_file:
    print(test_file.readlines())

# Запись файла
with open('new.txt', 'w') as new_file:
    new_file.write("First line in the new file \n")

with open('new.txt') as new_file:
    print(new_file.read())

with open('new.txt', 'a') as new_file:
    new_file.write("second line in the new file \n")

with open('new.txt') as new_file:
    print(new_file.read())

# Удаление файла

from pathlib import Path

print(Path('new.txt').exists())

Path('new.txt').unlink()

print(Path('new.txt').exists)
