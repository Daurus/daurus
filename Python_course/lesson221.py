# Lesson221 Проверка корректности email

import re


def check_email(email):
    email_regexp = r"^[a-zA-Z0-9_.]+@[a-zA-Z0-9]+\.[a-zA-Z0-9-.]+$"
    email_check_pattern = re.compile(email_regexp)
    validation_result = "valid" if email_check_pattern.fullmatch(
        email) else "not valid"
    return (email, validation_result)


# Valid
print(check_email('bs@gmail.com'))
print(check_email('b_.s@gmail.com'))
print(check_email('b_.s@sub.gmail.com'))
# Invaild
print(check_email('bsgmail.com'))
print(check_email('bs@gmailcom'))
print(check_email('@gmail.com'))
print(check_email('bs@'))
