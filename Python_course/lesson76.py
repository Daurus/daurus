# Lesson76 Диапазоны


# Диапазаон - упорядоченная неизменяемая последовательность элеменов

my_range = range(10, 20, 3)

print(type(my_range))

print(my_range)

print(list(my_range))

print(my_range[0])
print(my_range[1])
print(my_range[2])
print(my_range[3])

for n in my_range:
    print(n)
