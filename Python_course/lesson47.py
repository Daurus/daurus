# Lesson47 Методы списков

# Методы списков объекты наследуют из класса list
# append
# remove
# insert
# sort
# index
# clear
# copy
# extend
# reverse
# pop
# count

happy_smiles = []
happy_smiles.append('T')
happy_smiles.append('r')
happy_smiles.append('u')
happy_smiles.append('e')

print(happy_smiles)

# Удаление элементов из списка
happy_smiles.pop()
happy_smiles.pop(0)
print(happy_smiles)

# Запись удаленого в переменную
removed_element = happy_smiles.pop()
print(removed_element)

# Сортировка
posts_ids = [155, 215, 222, 178]

posts_ids.sort()

print(posts_ids)

posts_ids.sort(reverse=True)

print(posts_ids)
