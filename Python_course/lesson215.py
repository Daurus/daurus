# Lesson215 Модуль Random

import random

print(random.random())

print(random.randint(1, 199))

print(random.choice('abcd'))

print(random.choice([1, 10, 4]))

print(random.choice([1, 10, 4]))

print(random.choices([1, 10, 4, 5, 15], k=2))

# Смена порядка
my_list = [1, 10, 4, 5, 15]
print(random.shuffle(my_list))
print(my_list)
print(random.shuffle(my_list))
print(my_list)

print(''.join(random.choices('0123456789ABCDAEFR', k=8)))
