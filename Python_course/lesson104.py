# Lesson104 Области видимости в Python

# Глобальная область видимости / Локальная область

a = 10


def my_fn():
    a = True
    b = 15
    print(a)
    print(b)


my_fn()

print(a)
print(b)
# NameError: name 'b' is not defined

# Типы областей
# Глобальньная область видимости
# Область видимости функции
