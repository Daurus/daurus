# Lesson57 Несуществующие ключи словарей

my_motrobike = {
    'brand': 'Ducati',
    'price': 25000,
    'engine_vol': 1.2
}

print(my_motrobike.get('model'))

print(my_motrobike.get('price'))

print(my_motrobike.get('qty', 'privet'))
