# Lesson42 Практика

db_is_available = False

print(db_is_available)

print(type(db_is_available))

db_is_available = True

print(db_is_available)

# Конвертация в логический тип
print(bool(10))

print(bool('abc'))

print(bool([1, 2]))

print(bool([]))

print(bool(None))

print(100 > 10)

print('Long string' > 'long')

print([] == [])

print({'a': 3} == {'a': 3})
