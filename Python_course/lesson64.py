# Lesson64 Котрежи (Tuple)

# Кортежи неизменяемые

my_fruits = ('apple', 'banana', 'lime')

other_fruits = ('banana', 'apple', 'lime')

posts_ids = (151, 245, 765, 222)

print(len(other_fruits))

print(posts_ids[0])

# posts_ids[0] = 555

my_nums = (10, 5, 100, 0)

print(type(my_nums))

# my_nums[1] = 7

# del my_nums[1]


# Переменные кортежей

all_fruits = (my_fruits, other_fruits)

print(all_fruits)

# Несуществующие элементы

# print(posts_ids[10])

# Объединие кортежей

internal_ids = (151, 245)
external_ids = (625, 451, 999)

all_ids = internal_ids + external_ids

print(all_ids)
