# Lesson232 Работа с аргументами программы с модулем sys

import sys

print(sys.argv)

if len(sys.argv) < 3:
    raise IOError("you must provide username or password")

# username = sys.argv[1]
# password = sys.argv[2]

filename, username, password = sys.argv

print(username, password)
